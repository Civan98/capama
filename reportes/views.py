from django.http import HttpResponse,  HttpResponseRedirect
import datetime
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from reportes.models import Usuario, Reporte
from reportes.forms import Estatus


def registro(request):
    
    return render (request, 'registro.html')

"""def reportes(request):
 
    return render (request, 'reportes.html')"""

    #en nuestro caso principal es reportes

def login(request):
    #lista para almacenar posibles errores
    errores = []
    if 'member_id' in request.session:
        return HttpResponseRedirect('/reportes/')
    if request.method == 'POST':
        if not request.POST.get('usuario',''):
            errores.append('Introduce tu nombre de usuario')
        if not request.POST.get('password',''):
            errores.append('Introduce tu contraseña')
        if not errores:
            try:
                #obtenemos los datos del usuario desde la BD
                validar = Usuario.objects.get(Nombre = request.POST['usuario'])
            except Usuario.DoesNotExist:
                errores.append('¡El nombre de usuario no existe!')
            else:
                if validar.Password == request.POST['password']:
                    request.session['member_id'] = validar.id
                    #request.session es un diccionario de datos, en el cual llenamos con información del usuario
                    return HttpResponseRedirect('/reportes/')
                errores.append('La contraseña no es correcta')
    #si el usuario intenta ir al login, se valida si la sesión está activa, si es así, se le manda a la página principal

    return render(request,'login.html',{'errores':errores})

def reportes(request):
    #aquí validamos que exista una sesión para que el usuario pueda entrar a la página principal
    if 'member_id' in request.session:
        #errores = [] almacena los errores ocurridos
        nusuario = Usuario.objects.get(id = request.session['member_id'])#optiene todos los datos del usuario que esta en sesion
        usu = getattr(nusuario,'Nombre')#obtiene el nombre y lo almacena en la variable
        reporte = Reporte.objects.all()#optiene todos los datos de reporte
        Context = {'reportes':reporte, 'name': usu,}# guarda en el contexto los datos optenidos

        if request.method == "POST":#se activa cuando pulsan el submit del formulario
            nuevoestatus = Estatus(request.POST)#optiene lo escrito en el input y lo almacena
            if nuevoestatus.is_valid():# valida que el campo este bien
                form_data = nuevoestatus.cleaned_data#vacia los datos del input en la variable
                #abc = form_data.get("Estatus")#optiene el campo EstadoReporte y lo almacena ESTE ES USANDO EL FORM DE DJANGO 
                abc = request.POST['Estatus']#  se metio esta linea para poder acceder al valor de los inputs del radio
                nombre = request.POST['Nombre']#optiene el nombre del que se va a editar
            
                obj = Reporte.objects.get(NombreUsuario=nombre)#iguala lo que se edita con el nombre que se quiere editar
                obj.Estado_reporte = abc # asigna el valor      
                obj.save()#guarda el modelo
                
                Context = {'reportes':reporte, 'name': usu, 'form': nuevoestatus}
                return render (request,'reportes.html', Context )
        else:
            nuevoestatus = Estatus()#para mostrar el formulario 
            Context = {'reportes':reporte, 'name': usu, 'form': nuevoestatus}
            return render(request,'reportes.html', Context)
        
    return HttpResponseRedirect('/login/')

'''def buscar (request):

    if request.GET['Estatus']:
        #mensaje="articulo buscado: %r"%request.GET['Estatus']
        NEstado = request.GET['Estatus']
        Estados = Reporte.objects.filter(Estado_reporte__icontains=NEstado)
        return render(request, "resultado.html", {"Estados":Estados, "query":NEstado})

    else:
        mensaje = "Introduce algo en el campo"
    return HttpResponse(mensaje)
'''


def logOut(request):
    try:
         del request.session['member_id']
    except KeyError:
             pass
    return HttpResponseRedirect('/login/')