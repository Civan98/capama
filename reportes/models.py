from django.db import models

# Create your models here.
#tabla usuarios
class Usuario(models.Model):
   Nombre = models.CharField(max_length=40)
   Apellidos = models.CharField(max_length=60)
   puesto = models.CharField(max_length=40)
   Num_telefonico = models.CharField(max_length=10)
   Email = models.EmailField()
   Password = models.CharField(max_length=15)
   

#tabla de los reportes
class Reporte(models.Model):
   NombreUsuario = models.CharField(max_length=25, blank=True, null=True)
   ApellidoUsuario = models.CharField(max_length=60, blank=True, null=True)
   Direccion = models.CharField(max_length=60, blank=True, null=True)
   Num_contrato = models.CharField(max_length=10, blank=True, null=True)
   Prioridad = models.CharField(max_length=5, blank=True, null=True)
   Estado_reporte = models.CharField(max_length=20, blank=True, null=True)
   IdUsuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, blank=True, null=True)
   Foto = models.ImageField(upload_to="fuga", blank=True, null=True)