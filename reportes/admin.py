from django.contrib import admin
from reportes.models import Usuario, Reporte

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Reporte)