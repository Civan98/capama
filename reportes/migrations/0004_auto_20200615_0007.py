# Generated by Django 3.0.2 on 2020-06-15 05:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reportes', '0003_auto_20200614_1653'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reporte',
            old_name='Num_reporte',
            new_name='Num_contrato',
        ),
        migrations.AddField(
            model_name='reporte',
            name='Estado_reporte',
            field=models.CharField(max_length=20, null=True),
        ),
    ]
